/**
 * @file
 */

(function ($) {

/**
 * Webform Validations.
 */
Drupal.behaviors.custom = {
  attach: function (context, settings) {
      $('#edit-submitted-health-history-personal-information-no-children-1').change(function (e) {
        if ($(this).is(':checked')) {
          // console.log('Enter');.
         $('.webform-component-childrens-names-and-ages').fadeOut();
      }
       else {
        // console.log('Bye Bye');.
        $('.webform-component-childrens-names-and-ages').fadeIn();
      }
    });
    jQuery('.webform-component--consent-form--hippa-information').attr('id','tabHIPPA');
    jQuery('.webform-component--consent-form--ins-information').attr('id','tabINS');
    jQuery('.webform-component--consent-form--roi-information').attr('id','tabROI');

    jQuery('.confirmation input.form-checkbox').click(function () {
      if (jQuery(this).prop('checked')) {
        jQuery('.consent #mainTabs li.active').next().children('a').trigger('click');
      }
    });
  }
 }
})(jQuery);
