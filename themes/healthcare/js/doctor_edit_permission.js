/**
 * @file
 */

(function ($) {

/**
 * Webform Validations.
 */
Drupal.behaviors.user_password_popup = {
  attach: function (context, settings) {
      $('.webform-submit').click(function (e) {
        e.preventDefault();
        $('.modal').show();
        $('.fade').css('opacity',1);
        var visibility = $('.modal').css('display');
        if (visibility == 'block') {
            $('button.submit-form').click(function () {
              $('form').submit();
            });
         $('.close').click(function () {
           $('.modal').hide();
         });
        }
      });
  }
 }
})(jQuery);
