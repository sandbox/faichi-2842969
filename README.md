PATIENT INTAKE MANAGEMENT
--------------------------------
Patient Intake Management distribution is a solution that digitizes the patient registration process.
Healthcare industry has leveraged innovative technology solutions to improve patient experience across care continuum. Moreover, the governments and the regulatory authorities have ensured that there is a perfect balance between the adoption of technologies in healthcare and privacy of patient information.
Installation profiles define additional steps that run after the base
installation provided by Drupal core when Drupal is first installed.

USER MANAGEMENT
-----------------------
This distribution provides two user roles - patient and physician.
You need to create users for these roles.
User with patient role need to fill up 'Patient Registration Form'. After thant only physician will be able to view/edit patient's details.

'Patient Registration Form' is set as homepage for logged in users.

MORE INFORMATION
----------------
Refer to the "Installation profiles" section of the README.txt in the Drupal
root directory for further information on extending Drupal with custom profiles.
