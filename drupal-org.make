; patient_intake_management make file for d.o. usage
core = "7.x"
api = "2"
projects[drupal][type] = "core"
projects[drupal][version] = 7.53

; +++++ Modules +++++

projects[ctools][version] = "1.12"
projects[ctools][subdir] = "contrib"

projects[clientside_validation][version] = "1.44"
projects[clientside_validation][subdir] = "contrib"

projects[references][version] = "2.1"
projects[references][subdir] = "contrib"

projects[select_or_other][version] = "2.22"
projects[select_or_other][subdir] = "contrib"

projects[node_export][version] = "3.1"
projects[node_export][subdir] = "contrib"

projects[form_builder][version] = "1.20"
projects[form_builder][subdir] = "contrib"

projects[less][version] = "4.0"
projects[less][subdir] = "contrib"

projects[libraries][version] = "2.3"
projects[libraries][subdir] = "contrib"

projects[menu_item_visibility][version] = "1.0-beta2"
projects[menu_item_visibility][subdir] = "contrib"

projects[node_export_webforms][version] = "1.0-rc4"
projects[node_export_webforms][subdir] = "contrib"

projects[webform_wrappers][version] = "1.0-beta1"
projects[webform_wrappers][subdir] = "contrib"

projects[uuid][version] = "7.x-1.0-beta2"
projects[uuid][subdir] = "contrib"

projects[jquery_update][version] = "3.0-alpha3"
projects[jquery_update][subdir] = "contrib"

projects[options_element][version] = "1.12"
projects[options_element][subdir] = "contrib"

projects[views][version] = "3.14"
projects[views][subdir] = "contrib"

projects[webform][version] = "4.14"
projects[webform][subdir] = "contrib"

projects[webform_matrix_component][version] = "4.27"
projects[webform_matrix_component][subdir] = "contrib"

projects[webform_table_element][version] = "1.7"
projects[webform_table_element][subdir] = "contrib"

projects[features][version] = "7.x-2.10"
projects[features][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[webform_features][version] = "7.x-3.0-beta3"
projects[webform_features][subdir] = "contrib"

; +++++ TODO modules without versions +++++

projects[patient_assessor][type] = "module"
projects[patient_assessor][subdir] = "custom"

; +++++ Themes +++++

; bootstrap
projects[bootstrap][type] = "theme"
projects[bootstrap][version] = "3.10"
projects[bootstrap][subdir] = "contrib"

projects[healthcare][type] = "theme"
projects[healthcare][subdir] = "custom"

