<?php
/**
 * @file
 * patient_assessor.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function patient_assessor_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  // Exported role: patient.
  $roles['patient'] = array(
    'name' => 'patient',
    'weight' => 3,
  );

  // Exported role: physician.
  $roles['physician'] = array(
    'name' => 'physician',
    'weight' => 4,
  );

  return $roles;
}
