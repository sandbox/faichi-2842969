<?php
/**
 * @file
 * patient_assessor.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function patient_assessor_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: user-menu_log-out:user/logout.
  $menu_links['user-menu_log-out:user/logout'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user/logout',
    'router_path' => 'user/logout',
    'link_title' => 'Log out',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'user-menu_log-out:user/logout',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 10,
    'customized' => 0,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Log out');

  return $menu_links;
}
