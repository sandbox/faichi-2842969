/**
 * @file
 * Custom JavaScript for healthcare module.
 */

(function ($) {
    $(document).ready(function () {
        var formval = $('input[name=form_id]').val();
        if (formval == 'webform_client_form_1') {
            $('.webform-submit').prop('disabled', true);
            $('.webform-submit').click(function () {
                var c = confirm("Are you sure you want to Submit?");
                return c;
            });

            var hippaconf = $($('.webform-component--hippa-confirmation').contents().get(0)).text();
            var insconf = $($('.webform-component--ins-confirmation').contents().get(0)).text();
            var roiconf = $($('.webform-component--roi-confirmation').contents().get(0)).text();
            if ($.trim(hippaconf) == 'I Agree' && $.trim(insconf) == 'I Agree' && $.trim(roiconf) == 'I Agree') {
                $(".webform-submit").removeAttr('disabled');
            }
            var insuredname = 'edit-submitted-insurance-information-insurance-information-wrapper-2-insured';
            // Disabled reapitive fields.
            var attributecname = 'edit-submitted-health-history-personal-information-pi-wrapper-left-health-history';
            $('#' + attributecname + '-patient-name').prop("readonly", true);
            $('#' + attributecname + '-birth-date-month').prop("disabled", true);
            $('#' + attributecname + '-birth-date-day').prop("disabled", true);
            $('#' + attributecname + '-birth-date-year').prop("disabled", true);
            $('#' + attributecname + '-age').prop("readonly", true);
            $('#' + insuredname + '-social-security-number').prop("readonly", true);

            $('#' + insuredname + '-date-of-birth-month').prop("disabled", true);
            $('#' + insuredname + '-date-of-birth-day').prop("disabled", true);
            $('#' + insuredname + '-date-of-birth-year').prop("disabled", true);
            $('#edit-submitted-personal-information-personal-information-wrapper-2-age').prop("readonly", true);
            var pnamenext = $(".pname").val();
            var pdbnext = $(".pdateofbirth").val();
            pdbnext = pdbnext.split(',');
            var patientage = $(".patientage").val();
            var pmaritalstatus = $(".pmaritalstatus").val();
            var ssnumber = $(".ssnumbar").val();
            ssnumber = ssnumber.split(' ');
            var pnameexist = $('#' + attributecname + '-patient-name').val();
                $('#' + attributecname + '-patient-name').val(pnamenext);
                $('#' + attributecname + '-birth-date-month').val(pdbnext[0]);
                $('#' + attributecname + '-birth-date-day').val(pdbnext[1]);
                $('#' + attributecname + '-birth-date-year').val(pdbnext[2]);
                $('#' + attributecname + '-age').val(patientage);
            $(':radio[value=health_histroy_' + pmaritalstatus + ']').attr('checked', 'checked');

                $('#' + insuredname + '-date-of-birth-month').val(pdbnext[0]);
                $('#' + insuredname + '-date-of-birth-day').val(pdbnext[1]);
                $('#' + insuredname + '-date-of-birth-year').val(pdbnext[2]);

                $('#edit-submitted-insurance-information-insurance-information-wrapper-2-insurance-social-security-number-ssn3').val(ssnumber[0]);
                $('#edit-submitted-insurance-information-insurance-information-wrapper-2-insurance-social-security-number-ssn2').val(ssnumber[1]);
                $('#edit-submitted-insurance-information-insurance-information-wrapper-2-insurance-social-security-number-ssn4').val(ssnumber[2]);

            // Age calculation from date of birth.
            $("#edit-submitted-personal-information-personal-information-wrapper-date-of-birth-year").change(function () {
                var year = $('#edit-submitted-personal-information-personal-information-wrapper-date-of-birth-year').val();
                var month = $('#edit-submitted-personal-information-personal-information-wrapper-date-of-birth-month').val();
                var day = $('#edit-submitted-personal-information-personal-information-wrapper-date-of-birth-day').val();
                month = month.length === 1 ? '0' + month : month;
                day = day.length === 1 ? '0' + day : day;
                var birthday = year + '-' + month + '-' + day;// '1988-04-07'.
                var now = new Date();
                var past = new Date(birthday);
                var nowYear = now.getFullYear();
                var pastYear = past.getFullYear();
                var age = nowYear - pastYear;
                    $('#edit-submitted-personal-information-personal-information-wrapper-2-age').val(age);
            });
        }

    });
})(jQuery);
